Untitled
- Delay -
active: false
- Duration - 
lowMin: 5000.0
lowMax: 5000.0
- Count - 
min: 5
max: 50
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 5.0
highMax: 5.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.3
timelineCount: 3
timeline0: 0.0
timeline1: 0.66
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: ellipse
edges: false
side: both
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 64.0
highMax: 64.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 64.0
highMax: 64.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 32.0
highMax: 32.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 12
colors0: 0.8901961
colors1: 0.10980392
colors2: 0.043137256
colors3: 0.77254903
colors4: 0.8039216
colors5: 0.039215688
colors6: 0.043137256
colors7: 0.6509804
colors8: 0.8901961
colors9: 0.87058824
colors10: 0.043137256
colors11: 0.59607846
timelineCount: 4
timeline0: 0.0
timeline1: 0.2685026
timeline2: 0.62650603
timeline3: 0.99827886
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.75
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2
timeline2: 0.8
timeline3: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
premultipliedAlpha: true
- Image Path -
Fragment.png
